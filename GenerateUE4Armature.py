import bpy


scene = bpy.context.scene
#scene.layers = [True] * 20 # Show all layers #2.8 has a new layer system so...
bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.select_all(action='DESELECT')

#----------------- Set cursor to center
for area in bpy.context.screen.areas: 
    if area.type == 'VIEW_3D': 
        region = area.spaces[0].region_3d
        override = bpy.context.copy()
        override['area'] = area
        override['region'] = region 
        bpy.ops.view3d.snap_cursor_to_center.poll(override)
        bpy.ops.view3d.snap_cursor_to_center(override)
   
bpy.ops.object.add(type='ARMATURE')
ob = bpy.context.object
ob.name = 'Armature'
ob.select = True
bpy.context.scene.objects.active = ob
arm = ob.data

for obj in scene.objects:
   if obj.name == 'Armature':
        scene.objects.active = obj
        bpy.ops.object.mode_set(mode='EDIT')

bones = {}

#----------------- Spine
bone = arm.edit_bones.new('pelvis')
bone.head[:] = 1.3536841578012896e-28, 1.0561532974243164, 96.75060272216797
bone.tail[:] = 1.2988678008696297e-06, 0.16524696350097656, 107.55630493164062
bone.roll = 90.00002299504568
bone.use_connect = False

bone = arm.edit_bones.new('spine_01')
bone.head[:] = 1.2988679145564674e-06, 0.16524696350097656, 107.55630493164062
bone.tail[:] = 3.547266715031583e-06, 1.5160138607025146, 126.76314544677734
bone.roll = 90.00000933466734
bone.use_connect = False
bone.parent = arm.edit_bones['pelvis']

bone = arm.edit_bones.new('spine_02')
bone.head[:] = 3.547266715031583e-06, 1.5160138607025146, 126.76314544677734
bone.tail[:] = 5.141274868947221e-06, 3.4979398250579834, 140.02984619140625
bone.roll = 90.00000933466734
bone.use_connect = True
bone.parent = arm.edit_bones['spine_01']

bone = arm.edit_bones.new('spine_03')
bone.head[:] = 5.141274868947221e-06, 3.4979398250579834, 140.02984619140625
bone.tail[:] = 3.2382908443651104e-07, 3.7984933853149414, 153.60784912109375
bone.roll = 90.00000933466734
bone.use_connect = True
bone.parent = arm.edit_bones['spine_02']

bone = arm.edit_bones.new('neck_01')
bone.head[:] = 7.170370281528449e-06, 5.874689102172852, 156.42100524902344
bone.tail[:] = 8.263858944701497e-06, 3.977632522583008, 165.51600646972656
bone.roll = 90.00000933466734
bone.use_connect = False
bone.parent = arm.edit_bones['spine_03']


#----------------- ArmL
bone = arm.edit_bones.new('clavicle_l')
bone.head[:] = 3.781989812850952, 2.760394334793091, 152.2012176513672
bone.tail[:] = 17.700223922729492, 9.711005210876465, 149.53024291992188
bone.roll = -173.9659153678436
bone.use_connect = False
bone.parent = arm.edit_bones['spine_03']

bone = arm.edit_bones.new('upperarm_l')
bone.head[:] = 17.700223922729492, 9.711005210876465, 149.53024291992188
bone.tail[:] = 27.643630981445312, 10.828948974609375, 137.79762268066406
bone.roll = -131.8732353622392
bone.use_connect = True
bone.parent = arm.edit_bones['clavicle_l']

bone = arm.edit_bones.new('upperarm_twist_01_l')
bone.head[:] = 27.64362907409668, 10.828948974609375, 137.79762268066406
bone.tail[:] = 37.26462173461914, 11.91064453125, 126.4454345703125
bone.roll = -131.87322170186087
bone.use_connect = False
bone.parent = arm.edit_bones['upperarm_l']

bone = arm.edit_bones.new('lowerarm_l')
bone.head[:] = 37.26462173461914, 11.91064453125, 126.44544219970703
bone.tail[:] = 47.3234977722168, 5.903029441833496, 118.78204345703125
bone.roll = -153.10849839748394
bone.use_connect = False
bone.parent = arm.edit_bones['upperarm_twist_01_l']

bone = arm.edit_bones.new('lowerarm_twist_01_l')
bone.head[:] = 47.3234977722168, 5.903029441833496, 118.78204345703125
bone.tail[:] = 56.646026611328125, 0.33520227670669556, 111.67963409423828
bone.roll = -153.1084847371056
bone.use_connect = False
bone.parent = arm.edit_bones['lowerarm_l']


#----------------- HandL
bone = arm.edit_bones.new('hand_l')
bone.head[:] = 56.646026611328125, 0.33520227670669556, 111.67964172363281
bone.tail[:] = 62.722206115722656, -3.4092471599578857, 104.19842529296875
bone.roll = 134.36095418249792
bone.use_connect = False
bone.parent = arm.edit_bones['lowerarm_twist_01_l']

bone = arm.edit_bones.new('index_01_l')
bone.head[:] = 63.042327880859375, -6.766394138336182, 103.81495666503906
bone.tail[:] = 64.6720962524414, -8.094826698303223, 100.07840728759766
bone.roll = -173.28338822446256
bone.use_connect = False
bone.parent = arm.edit_bones['hand_l']

bone = arm.edit_bones.new('index_02_l')
bone.head[:] = 64.6720962524414, -8.094826698303223, 100.07840728759766
bone.tail[:] = 65.43616485595703, -8.762378692626953, 96.8398208618164
bone.roll = -158.983349008287
bone.use_connect = True
bone.parent = arm.edit_bones['index_01_l']

bone = arm.edit_bones.new('middle_01_l')
bone.head[:] = 64.49005126953125, -4.479480266571045, 103.48135375976562
bone.tail[:] = 66.53084564208984, -5.724931716918945, 99.5042724609375
bone.roll = 168.87201492636143
bone.use_connect = False
bone.parent = arm.edit_bones['hand_l']

bone = arm.edit_bones.new('middle_02_l')
bone.head[:] = 66.53084564208984, -5.724931716918945, 99.5042724609375
bone.tail[:] = 67.43497467041016, -6.542197227478027, 96.06498718261719
bone.roll = -177.7656588861484
bone.use_connect = True
bone.parent = arm.edit_bones['middle_01_l']

bone = arm.edit_bones.new('pinky_01_l')
bone.head[:] = 64.0250244140625, 0.15892377495765686, 103.01749420166016
bone.tail[:] = 65.93714141845703, -0.12659110128879547, 100.01512145996094
bone.roll = 141.3682091765541
bone.use_connect = False
bone.parent = arm.edit_bones['hand_l']

bone = arm.edit_bones.new('pinky_02_l')
bone.head[:] = 65.93714141845703, -0.12659110128879547, 100.01512145996094
bone.tail[:] = 67.012451171875, -0.35460540652275085, 97.23920440673828
bone.roll = 154.76547496937238
bone.use_connect = True
bone.parent = arm.edit_bones['pinky_01_l']

bone = arm.edit_bones.new('ring_01_l')
bone.head[:] = 64.57562255859375, -2.0826382637023926, 103.03924560546875
bone.tail[:] = 66.59227752685547, -2.9754748344421387, 99.1970443725586
bone.roll = 152.8629257760653
bone.use_connect = False
bone.parent = arm.edit_bones['hand_l']

bone = arm.edit_bones.new('ring_02_l')
bone.head[:] = 66.59227752685547, -2.9754748344421387, 99.1970443725586
bone.tail[:] = 67.4341049194336, -3.5501813888549805, 95.8731689453125
bone.roll = 169.32514333627842
bone.use_connect = True
bone.parent = arm.edit_bones['ring_01_l']

bone = arm.edit_bones.new('thumb_01_l')
bone.head[:] = 57.477996826171875, -3.8766472339630127, 107.63908386230469
bone.tail[:] = 57.60747146606445, -7.0768537521362305, 105.46736145019531
bone.roll = -60.51705382001125
bone.use_connect = False
bone.parent = arm.edit_bones['hand_l']

bone = arm.edit_bones.new('thumb_02_l')
bone.head[:] = 57.60747146606445, -7.0768537521362305, 105.46736145019531
bone.tail[:] = 57.784175872802734, -9.566149711608887, 102.26215362548828
bone.roll = -59.769318860624104
bone.use_connect = True
bone.parent = arm.edit_bones['thumb_01_l']




#----------------- ArmR
bone = arm.edit_bones.new('clavicle_r')
bone.head[:] = -3.781996011734009, 2.760396718978882, 152.20132446289062
bone.tail[:] = -17.70016098022461, 9.710970878601074, 149.53036499023438
bone.roll = 173.9659563489786
bone.use_connect = False
bone.parent = arm.edit_bones['spine_03']

bone = arm.edit_bones.new('upperarm_r')
bone.head[:] = -17.70016098022461, 9.710970878601074, 149.53036499023438
bone.tail[:] = -27.643611907958984, 10.828922271728516, 137.79771423339844
bone.roll = 131.8732626829959
bone.use_connect = True
bone.parent = arm.edit_bones['clavicle_r']

bone = arm.edit_bones.new('upperarm_twist_01_r')
bone.head[:] = -27.643611907958984, 10.828922271728516, 137.79771423339844
bone.tail[:] = -37.26464080810547, 11.910614013671875, 126.44549560546875
bone.roll = 151.82516049395193
bone.use_connect = False
bone.parent = arm.edit_bones['upperarm_r']

bone = arm.edit_bones.new('lowerarm_r')
bone.head[:] = -37.26464080810547, 11.910614967346191, 126.44549560546875
bone.tail[:] = -47.32350540161133, 5.902980804443359, 118.78211212158203
bone.roll = 153.10853937861899
bone.use_connect = False
bone.parent = arm.edit_bones['upperarm_twist_01_r']

bone = arm.edit_bones.new('lowerarm_twist_01_r')
bone.head[:] = -47.323509216308594, 5.902981281280518, 118.78211212158203
bone.tail[:] = -56.64610290527344, 0.33510109782218933, 111.67964935302734
bone.roll = 166.61891310409266
bone.use_connect = False
bone.parent = arm.edit_bones['lowerarm_r']



#----------------- HandR
bone = arm.edit_bones.new('hand_r')
bone.head[:] = -56.64610290527344, 0.33510109782218933, 111.67965698242188
bone.tail[:] = -62.72213363647461, -3.4093387126922607, 104.19831848144531
bone.roll = -134.36089954098458
bone.use_connect = False
bone.parent = arm.edit_bones['lowerarm_twist_01_r']

bone = arm.edit_bones.new('index_01_r')
bone.head[:] = -63.0421142578125, -6.7664337158203125, 103.81486511230469
bone.tail[:] = -64.67208099365234, -8.094915390014648, 100.07820892333984
bone.roll = 173.28341554521924
bone.use_connect = False
bone.parent = arm.edit_bones['hand_r']

bone = arm.edit_bones.new('index_02_r')
bone.head[:] = -64.67208099365234, -8.094915390014648, 100.07820892333984
bone.tail[:] = -65.43624114990234, -8.762518882751465, 96.83965301513672
bone.roll = 158.98336266866536
bone.use_connect = True
bone.parent = arm.edit_bones['index_01_r']

bone = arm.edit_bones.new('middle_01_r')
bone.head[:] = -64.48995208740234, -4.4795379638671875, 103.48139953613281
bone.tail[:] = -66.53072357177734, -5.725015640258789, 99.50409698486328
bone.roll = -168.87198760560477
bone.use_connect = False
bone.parent = arm.edit_bones['hand_r']

bone = arm.edit_bones.new('middle_02_r')
bone.head[:] = -66.53072357177734, -5.725015640258789, 99.50409698486328
bone.tail[:] = -67.43487548828125, -6.542295455932617, 96.06475067138672
bone.roll = 177.7656862069051
bone.use_connect = True
bone.parent = arm.edit_bones['middle_01_r']

bone = arm.edit_bones.new('pinky_01_r')
bone.head[:] = -64.02491760253906, 0.15881149470806122, 103.01739501953125
bone.tail[:] = -65.93699645996094, -0.1267162561416626, 100.01490783691406
bone.roll = -141.3681681954191
bone.use_connect = False
bone.parent = arm.edit_bones['hand_r']

bone = arm.edit_bones.new('pinky_02_r')
bone.head[:] = -65.93699645996094, -0.1267162561416626, 100.01490783691406
bone.tail[:] = -67.01251983642578, -0.354654461145401, 97.23931121826172
bone.roll = -154.76543398823733
bone.use_connect = True
bone.parent = arm.edit_bones['pinky_01_r']

bone = arm.edit_bones.new('ring_01_r')
bone.head[:] = -64.57562255859375, -2.0827667713165283, 103.03901672363281
bone.tail[:] = -66.59220886230469, -2.9755287170410156, 99.19712829589844
bone.roll = -152.86289845530862
bone.use_connect = False
bone.parent = arm.edit_bones['hand_r']

bone = arm.edit_bones.new('ring_02_r')
bone.head[:] = -66.59220886230469, -2.9755287170410156, 99.19712829589844
bone.tail[:] = -67.43404388427734, -3.550236701965332, 95.87325286865234
bone.roll = -169.32511601552176
bone.use_connect = True
bone.parent = arm.edit_bones['ring_01_r']

bone = arm.edit_bones.new('thumb_01_r')
bone.head[:] = -57.47806167602539, -3.8767666816711426, 107.63892364501953
bone.tail[:] = -57.607418060302734, -7.076916694641113, 105.46731567382812
bone.roll = 60.51708114076793
bone.use_connect = False
bone.parent = arm.edit_bones['hand_r']

bone = arm.edit_bones.new('thumb_02_r')
bone.head[:] = -57.607418060302734, -7.076916694641113, 105.46731567382812
bone.tail[:] = -57.784122467041016, -9.56622314453125, 102.2621078491211
bone.roll = 59.76933252100244
bone.use_connect = True
bone.parent = arm.edit_bones['thumb_01_r']




#----------------- LegL
bone = arm.edit_bones.new('thigh_l')
bone.head[:] = 9.005809783935547, 0.5300275683403015, 95.29985046386719
bone.tail[:] = 11.710777282714844, 1.190050721168518, 73.3817367553711
bone.roll = -105.38767421389737
bone.use_connect = False
bone.parent = arm.edit_bones['pelvis']

bone = arm.edit_bones.new('thigh_twist_01_l')
bone.head[:] = 11.710777282714844, 1.190050721168518, 73.38174438476562
bone.tail[:] = 14.217845916748047, 1.8017860651016235, 53.06720733642578
bone.roll = -99.94126087916169
bone.use_connect = False
bone.parent = arm.edit_bones['thigh_l']

bone = arm.edit_bones.new('calf_l')
bone.head[:] = 14.217845916748047, 1.8017860651016235, 53.06720733642578
bone.tail[:] = 15.673959732055664, 4.996794700622559, 32.89370346069336
bone.roll = -97.13498203601357
bone.use_connect = False
bone.parent = arm.edit_bones['thigh_twist_01_l']

bone = arm.edit_bones.new('calf_twist_01_l')
bone.head[:] = 15.673959732055664, 4.996794700622559, 32.89370346069336
bone.tail[:] = 17.076255798339844, 8.073709487915039, 13.46585464477539
bone.roll = -100.07074077525762
bone.use_connect = False
bone.parent = arm.edit_bones['calf_l']

bone = arm.edit_bones.new('foot_l')
bone.head[:] = 17.076255798339844, 8.073709487915039, 13.46585464477539
bone.tail[:] = 17.908687591552734, -8.355310440063477, 2.811812162399292
bone.roll = -59.41234585384255
bone.use_connect = False
bone.parent = arm.edit_bones['calf_twist_01_l']


#----------------- LegR
bone = arm.edit_bones.new('thigh_r')
bone.head[:] = -9.005803108215332, 0.5300228595733643, 95.30003356933594
bone.tail[:] = -11.71077823638916, 1.1900471448898315, 73.38192749023438
bone.roll = 105.3876878742757
bone.use_connect = False
bone.parent = arm.edit_bones['pelvis']

bone = arm.edit_bones.new('thigh_twist_01_r')
bone.head[:] = -11.71077823638916, 1.1900471448898315, 73.38192749023438
bone.tail[:] = -14.217875480651855, 1.8017908334732056, 53.06718444824219
bone.roll = 99.94104231310824
bone.use_connect = False
bone.parent = arm.edit_bones['thigh_r']

bone = arm.edit_bones.new('calf_r')
bone.head[:] = -14.217876434326172, 1.8017909526824951, 53.06718444824219
bone.tail[:] = -15.674003601074219, 4.996819972991943, 32.89354705810547
bone.roll = 97.13499569639191
bone.use_connect = False
bone.parent = arm.edit_bones['thigh_twist_01_r']

bone = arm.edit_bones.new('calf_twist_01_r')
bone.head[:] = -15.674003601074219, 4.996819972991943, 32.89354705810547
bone.tail[:] = -17.076292037963867, 8.073732376098633, 13.465706825256348
bone.roll = 97.60359448477897
bone.use_connect = False
bone.parent = arm.edit_bones['calf_r']

bone = arm.edit_bones.new('foot_r')
bone.head[:] = -17.076292037963867, 8.073732376098633, 13.465707778930664
bone.tail[:] = -17.908721923828125, -8.355230331420898, 2.811681032180786
bone.roll = 59.41388264640581
bone.use_connect = False
bone.parent = arm.edit_bones['calf_twist_01_r']




#----------------- Set scale in object mode
ob.scale = (0.01,0.01,0.01)
bpy.ops.object.mode_set(mode='OBJECT')
scene.objects.active = ob
bpy.context.scene.objects.active = ob
bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)